from django.db import models
from django.contrib.postgres.fields import JSONField

from .purchaseorder import PurchaseOrder


class WorkOrder(models.Model):
    create_date = models.DateField()
    data = JSONField()

    def __str__(self):
        return f'<WorkOrder: {self.create_date}>'

    