from django.db import models

from .company import Company
from .pogroup import POGroup


class PurchaseOrder(models.Model):
    create_date = models.DateField()
    detail = models.CharField(max_length=512)

    cost = models.DecimalField(max_digits=8, decimal_places=2)

    # Computed from work-orders. Also stored here for effecincy
    recognized = models.DecimalField(max_digits=8, decimal_places=2)

    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    po_group = models.ForeignKey(POGroup, null=True, on_delete=models.SET_NULL)


    def balance(self):
        return self.cost - self.recognized

    def __str__(self):
        return f'<PO: {self.detail}>'
