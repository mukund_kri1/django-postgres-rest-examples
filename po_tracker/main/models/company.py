from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=512)

    class Meta:
        verbose_name_plural = 'companies'

        
    def __str__(self):
        return f'<Company: {self.name}>'


