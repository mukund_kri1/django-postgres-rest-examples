from django.db import models

from .company import Company


class POGroup(models.Model):
    title = models.CharField(max_length=512)
    company = models.ForeignKey(Company, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = 'PO groups'

    def __str__(self):
        return f'<Group: {self.title}>'
