import json 

from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status 
from django.urls import reverse

from main.models import Company, POGroup 


class POGroupAPITest(APITestCase):

    fixtures = ['companies', 'po-groups']

    def test_get_all_pogroups(self):
        url = reverse('pogroup-list')
        self.assertEqual(url, '/pogroup/')

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = json.loads(response.content)
        self.assertEqual(len(json_response), 1)
