import json 

from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse

from main.models import Company


class CompanyAPITest(APITestCase):

    fixtures = ['companies.json']

    def test_get_all_companies(self):
        url = reverse('company-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = json.loads(response.content)
        self.assertEqual(len(json_response), 1)


    def test_post_companies(self):
        url = reverse('company-list')
        response = self.client.post(url, {'name': 'Regalix'}, format='json')

        self.assertEqual(Company.objects.count(), 2)
