from django.test import TestCase

from main.models import Company, POGroup, PurchaseOrder


class PurchaseOrderModelTest(TestCase):

    fixtures = ['companies', 'po-groups', 'purchase-orders']

    def test_string_representation(self):
        entry = PurchaseOrder(detail='Video Editing + PWTs')
        self.assertEqual(str(entry), '<PO: Video Editing + PWTs>')

