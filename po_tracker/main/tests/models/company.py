from django.test import TestCase

from main.models import Company 


class CompanyModelTest(TestCase):

    fixtures = ['companies.json']

    def test_string_representation(self):
        entry = Company(name="Vmware")
        self.assertEqual(str(entry), '<Company: Vmware>')

    def test_with_real_database(self):
        entry = Company.objects.get(pk=1)
        self.assertEqual(str(entry), '<Company: VMWare>')
