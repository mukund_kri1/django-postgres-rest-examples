from django.test import TestCase

from main.models import Company, POGroup


class POGroupModelTest(TestCase):

    fixtures = ['companies', 'po-groups']

    def test_string_representation(self):
        entry = POGroup(title='Dave/Marty')
        self.assertEqual(str(entry), '<Group: Dave/Marty>')

    def test_with_real_database(self):
        entry = POGroup.objects.get(pk=1)
        self.assertEqual(str(entry), '<Group: Dave/Marty>')
        self.assertEqual(str(entry.company), '<Company: VMWare>')

