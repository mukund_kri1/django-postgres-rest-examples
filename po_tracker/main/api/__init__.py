from rest_framework import routers

from .company import CompanyViewSet
from .pogroup import POGroupViewSet
from .purchaseorder import PurchaseOrderViewSet
from .workorder import WorkOrderViewSet


router = routers.DefaultRouter()
router.register(r'company', CompanyViewSet)
router.register(r'pogroup', POGroupViewSet)
router.register(r'purchaseorder', PurchaseOrderViewSet)
router.register(r'workorder', WorkOrderViewSet)
