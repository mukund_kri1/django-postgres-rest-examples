from rest_framework import serializers, viewsets

from main.models import POGroup


class POGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = POGroup
        fields = '__all__'


class POGroupViewSet(viewsets.ModelViewSet):
    queryset = POGroup.objects.all()
    serializer_class = POGroupSerializer
