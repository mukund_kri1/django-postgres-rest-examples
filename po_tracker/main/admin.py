from django.contrib import admin

from .models import Company, POGroup, PurchaseOrder, WorkOrder


admin.site.register(Company)
admin.site.register(POGroup)
admin.site.register(PurchaseOrder)
admin.site.register(WorkOrder)
