from django.urls import path, include

from .api import router


urlpatterns = [

    # URLs of the main app
    path('', include(router.urls)),

]