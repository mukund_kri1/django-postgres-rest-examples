from django.db import models


class Book(models.Model):
    ''' Represent a book entry in the database '''
    isbn = models.CharField(primary_key=True, max_length=15)
    title = models.CharField(max_length=512)
    description = models.CharField(max_length=4096)
    authors = models.CharField(max_length=1024)

    def __str__(self):
        return f'<Book: {self.title}>'

