from rest_framework import serializers, viewsets, routers

from .models import Book


class BookSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Book
        fields = ('isbn', 'title', 'description', 'authors')

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


router = routers.DefaultRouter()
router.register(r'book', BookViewSet)