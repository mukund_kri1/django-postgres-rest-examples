from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),

    # rest-framework urls for easy development 
    path('api-auth/', include('rest_framework.urls')),

    # main app routes, hook them on to the root url
    path('', include('main.urls'))
]
