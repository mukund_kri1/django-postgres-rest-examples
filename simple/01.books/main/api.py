from rest_framework import routers, serializers, viewsets

from .models import Author, Book, Shelf


router = routers.DefaultRouter()


class AuthorSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Author
        fields = ('first_name', 'last_name', 'middle_name', 'book_set')

class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

router.register(r'author', AuthorViewSet)


class BookSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Book
        fields = ('isbn', 'title', 'description', 'authors', 'shelf')

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

router.register(r'book', BookViewSet)


class ShelfSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Shelf
        fields = ('tag', 'book_set')

class ShelfViewSet(viewsets.ModelViewSet):
    queryset = Shelf.objects.all()
    serializer_class = ShelfSerializer

router.register(r'shelf', ShelfViewSet)

