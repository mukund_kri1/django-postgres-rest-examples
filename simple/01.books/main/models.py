from django.db import models


class Author(models.Model):

    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)

    def __str__(self):
        return f"<Author: {self.first_name} {self.last_name}>"


class Shelf(models.Model):
    '''
    Each self carries many books.
    '''
    tag = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"<Shelf: {self.tag}>"


class Book(models.Model):
    '''
    Each record represents a physical book
    '''
    isbn = models.CharField(unique=True, max_length=15)
    title = models.CharField(max_length=512)
    description = models.TextField()

    shelf = models.ForeignKey(Shelf, null=True, on_delete=models.SET_NULL)
    authors = models.ManyToManyField(Author)

    def __str__(self):
        return f"<Book: {self.title}>"


