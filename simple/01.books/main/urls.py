from django.urls import path, include

from .views import index
from .api import router


urlpatterns = [

    # URLs of the main app
    path('', include(router.urls)),

]