from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),

    # rest-framework urls for easy development 
    path('api-auth/', include('rest_framework.urls')),

    # URLs of the main app
    path('', include('main.urls')),

]
